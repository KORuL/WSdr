#include "Encode.h"

Encode::Encode(QObject *parent) : QObject(parent)
{
    Path = QCoreApplication::applicationDirPath() + "/_Out_PCM";
    QDir dir(Path);
    if(!dir.exists())
        dir.mkdir(Path);
    Path += "/";

    cnt = 0;
}

void Encode::EncodeFile(QString &str, QString &filein)
{
    QDir dir(Path);
    if(!dir.exists())
        dir.mkdir(Path);

    QFileInfo fileinf(filein);
    QString FileOut = Path + fileinf.fileName() + "_cnt_%1" + ".pcm";
    FileOut = FileOut.arg(cnt);
    cnt++;

    fileOut.setFileName(FileOut);
    fileIn .setFileName(str);
    if (fileIn.open(QIODevice::ReadOnly))
    {
        if (fileOut.open(QIODevice::WriteOnly))
        {

            unsigned int len      = fileIn.size();
            unsigned char* Buf    = new unsigned char[len];
                    short* BufOut;

            try
            {
                fileIn.read((char*)Buf, len);
                fileIn.close();
            }
            catch(...)
            {
                QString str1 = "Ошибка чтения файла сессии  " + filein;
                Q_EMIT Log(str1);

                delete []Buf;
                return;
            }

            unsigned int outLength = 0;
            BufOut = m_SND_AUD.DoSynchr((char*)Buf, len, outLength);

            fileOut.write((char*)BufOut, outLength);
            fileOut.close();

            QString str1 = "Декодирован файл   " + filein;
            Q_EMIT Log(str1);
            Q_EMIT Complited(filein);


            delete []Buf;
            delete []BufOut;
        }

        fileIn .close();
        fileOut.close();
    }
}
