#ifndef WSDR_H
#define WSDR_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTimer>
#include <QFileSystemWatcher>
#include <Parse.h>
#include <Encode.h>

namespace Ui {
class WSdr;
}

class WSdr : public QMainWindow
{
    Q_OBJECT

public:
    explicit WSdr(QWidget *parent = 0);
    ~WSdr();


public slots:
    void changed();
    void changed(const QString &path);
    void nextFile();
    void Log(QString str);
    void Complited(QString str);

    void SetMaxProgress(long i);
    void SetValueProgress(long i);

private slots:
    void on_PB_Start_clicked();
    void on_PB_Clear_clicked();

private:
    Ui::WSdr *ui;
    QFileSystemWatcher *wartcher;
    QStringList filesForProgress;

    Parse  *m_parse;
    Encode *encode;

    QString Path;

    QTimer timerRestart;
    QTimer timerRewatch;
};

#endif // WSDR_H
