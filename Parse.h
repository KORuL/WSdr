#ifndef PARSE_H
#define PARSE_H

#include <QObject>
#include <QDebug>
#include <QApplication>
#include "ParseSessions.h"
#include "Encode.h"

class Parse : public QObject
{
    Q_OBJECT
public:
    explicit Parse(Encode *encode, QObject *parent = 0);
    void ParseFile(QString FileName);    

    bool isWork() { return work; }

signals:
	void Log(QString str);
    void Restart();
    void setmaxpr(long i);
    void setvaluepr(long i);

private:
    bool work;
    Encode *m_encode;
};

#endif // PARSE_H
