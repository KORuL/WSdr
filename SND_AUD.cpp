#include "SND_AUD.h"
#include <QApplication>
#include "Adpcm.h"

SND_AUD::SND_AUD(QObject *parent) : QObject(parent)
{

}

short *SND_AUD::DoSynchr(char *Buf, unsigned int lenght, unsigned int &outlength)
{
    short *bufout = new short[2*lenght];
    ima_adpcm_state_t *state = new ima_adpcm_state_t;

    unsigned int offset   = 0;
    unsigned int cpy      = 0;
    unsigned short count  = 0;

    const char start  = 0x82;
    const char start1 = 0x7e;

    const char A = 0x41;
    const char U = 0x55;
    const char D = 0x44;

    const char S = 0x53;
    const char N = 0x4e;

    try {
        while(offset < lenght)
        {
            QApplication::processEvents();
            while(true && offset < lenght)
            {
                ///< 0x827e
                if( start == Buf[offset] && start1 == Buf[offset + 1] )
                {
                    offset += 2;
                    break;
                }
                else
                    offset++;
            }
            if(offset < lenght)
            {
                unsigned short len = Buf[offset];
                len  = len << 8;
                len  = (len & 0xFF00) ^ ((unsigned short)(Buf[offset + 1]) & 0x00FF);

                offset += 2;
                if( (S == Buf[offset] && N == Buf[offset+1] && D == Buf[offset+2]) )
                {
                    /// SND
                    offset += 4;

                    unsigned short count1 = Buf[offset+1];
                    count1  = count1 << 8;
                    count1  = (count1 & 0xFF00) ^ ((unsigned short)(Buf[offset]) & 0x00FF);

                    offset += 6;
                    len -= 10;

                    unsigned char *bufToDecode = (unsigned char *)Buf+offset;

                    if(count == 0 || count1 == count+1)
                    {
                        Adpcm::decode_ima_adpcm_e8_i16(bufToDecode, &bufout[cpy], len, state);
                        cpy    += 2*len;
                        offset += len;
                        count = count1;
                    }
                    else if(count1 > count+1)
                    {
//                        delete state;
//                        state = new ima_adpcm_state_t;
//                        state->previousValue = 0;
                        Adpcm::decode_ima_adpcm_e8_i16(bufToDecode, &bufout[cpy], len, state);
                        cpy    += 2*len;
                        offset += len;
                        count = count1;
                    }
                    else if(count1 < count+1)
                    {
                        offset += len;
                    }
                }
                else if( (A == Buf[offset] && U == Buf[offset+1] && D == Buf[offset+2]) )
                {
                    ///  AUD
                    offset += 4;
                    offset += 6;
                    len -= 10;

                    unsigned char *bufToDecode = (unsigned char *)Buf+offset;

                    Adpcm::decode_ima_adpcm_e8_i16(bufToDecode, &bufout[cpy], len, state);
                    cpy    += 2*len;
                    offset += len;
                }
                else
                    offset += len;
            }
        }
        outlength = cpy;
        delete state;
    } catch (...) {
        if(bufout)
            delete []bufout;
        bufout = NULL;

        delete state;
        outlength = 0;
    }

    return bufout;
}
