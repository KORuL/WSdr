﻿#ifndef __STRUCTS_H__
#define __STRUCTS_H__

#include <QObject>
#include <winsock2.h>
//#include <Ws2tcpip.h>
//#pragma comment(lib, "Ws2_32.lib")

QString ConvertIP(unsigned int IP)
{
	IP = ntohl(IP);
	return QString("%1.%2.%3.%4").arg((IP >> 24) & 0xFF,0,10).arg((IP >> 16) & 0xFF,0,10).arg((IP >> 8) & 0xFF,0,10).arg(IP & 0xFF,0,10);
}
QString ConvertIPwithZero(unsigned int IP)
{
	IP = ntohl(IP);
	return QString("%1.%2.%3.%4").arg((IP >> 24) & 0xFF,3,10,QChar('0')).arg((IP >> 16) & 0xFF,3,10,QChar('0')).arg((IP >> 8) & 0xFF,3,10,QChar('0')).arg(IP & 0xFF,3,10,QChar('0'));
}
QString ConvertPort(unsigned short Port)
{
	Port = ntohs(Port);
	return QString("%1").arg(Port,0,10);
}
QString ConvertPortwithZero(unsigned short Port)
{
	Port = ntohs(Port);
	return QString("%1").arg(Port,5,10,QChar('0'));
}
unsigned char MGW[4] = {0x4D, 0x20, 0x47, 0x57};

#define SWAP_INT(X) ((X >> 24) & 0xFF) | ((X >> 8) & 0xFF00) | ((X << 24) & 0xFF000000) |((X << 8) & 0xFF0000)  
#define SWAP_SHORT(X) ((X >> 8) & 0xFF) | ((X << 8) & 0xFF00)  

//#pragma pack(push, 1)

// -------------------------------------------------------------------
/// \brief TCP/UDP порты.
// -------------------------------------------------------------------
typedef union  UPorts
{
	// -------------------------------------------------------------------
	/// \brief Конструктор.
	// -------------------------------------------------------------------
	UPorts() : m_Port32Bit(0) {}
	// -------------------------------------------------------------------
	/// \brief Конструктор.
	// -------------------------------------------------------------------
	UPorts(unsigned int Value_) : m_Port32Bit(Value_) {}
	// -------------------------------------------------------------------
	/// \brief Конструктор.
	// -------------------------------------------------------------------
	UPorts(unsigned short PortSrc_, unsigned short PortDst_) : m_PortSrc(PortSrc_), m_PortDst(PortDst_) {}
	// -------------------------------------------------------------------
	/// \brief Оператор "=".
	// -------------------------------------------------------------------
	inline union UPorts& operator = (unsigned int Value_);
	// -------------------------------------------------------------------
	/// \brief Оператор "=".
	// -------------------------------------------------------------------
	inline union UPorts& operator = (const union UPorts &Value_);
	// -------------------------------------------------------------------
	/// \brief Оператор "==".
	// -------------------------------------------------------------------
	inline bool operator == (const UPorts& Other_) const;
	// -------------------------------------------------------------------
	/// \brief Оператор "!=".
	// -------------------------------------------------------------------
	inline bool operator != (const UPorts& Other_) const;
	// -------------------------------------------------------------------
	/// \brief Инициализация портов.
	// -------------------------------------------------------------------
	inline void init(unsigned short PortSrc_, unsigned short PortDst_);
	// -------------------------------------------------------------------
	/// \brief Перевод сетевого представления портов в машинное.
	// -------------------------------------------------------------------
	inline void network2Host();
	// -------------------------------------------------------------------
	/// \brief Возвращает "развернутый" порт источника.
	// -------------------------------------------------------------------
	unsigned short ntohsPortSrc() const { return ntohs(m_PortSrc); }
	// -------------------------------------------------------------------
	/// \brief Возвращает "развернутый" порт получателя.
	// -------------------------------------------------------------------
	unsigned short ntohsPortDst() const { return ntohs(m_PortDst); }

	unsigned int m_Port32Bit;
	struct
	{
		unsigned short m_PortSrc;
		unsigned short m_PortDst;
	};
} TPorts;
// -------------------------------------------------------------------
/// \brief Заголовок UDP пакета.
///
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|          Source Port          |       Destination Port         | 
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|           Length				|          Checksum              | 
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// -------------------------------------------------------------------
typedef struct SUDPHeader
{
	TPorts m_Ports;
	unsigned short m_Length;
	unsigned short m_CRC;

	unsigned int headerLength() const { return sizeof(SUDPHeader); }
	unsigned int length() const { return ntohs(m_Length); }
	//---------------------------------------------------------------------------
	/// \brief Проверка контрольной суммы.
	///
	/// \param IPAddrLen_ размер IP адреса в байтах (4 для IPv4, 16 для IPv6)
	//---------------------------------------------------------------------------
	bool checkCRC(unsigned short* IPSrcDst_, int DataSize_, int IPAddrLen_ = 4) const;
} TUDPHeader;

// -------------------------------------------------------------------
/// \brief Заголовок TCP пакета.
///
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|          Source Port          |       Destination Port         | 
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|                        Sequence Number                         | 
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|                    Acknowledgment Number                       |  
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|  Data |           |U|A|P|R|S|F|                                |
///		| Offset| Reserved  |R|C|S|S|Y|I|            Window              | 
///		|       |           |G|K|H|T|N|N|                                |
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|           Checksum            |         Urgent Pointer         | 
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|                    Options                    |    Padding     |
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
///		|                             data                               |
///		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// -------------------------------------------------------------------
typedef struct STCPHeader
{
	TPorts m_Ports;
	unsigned int m_SeqNumber;
	unsigned int m_AckNumber;
	unsigned char m_Offset;
	unsigned char m_Flags;
	unsigned short m_Windows;
	unsigned short m_CRC;
	unsigned short m_UrgentPointer;

	unsigned int headerLength() const { return 4 * (m_Offset >> 4); }
	unsigned int sequenceNumber() const { return ntohl(m_SeqNumber); }
	unsigned int ack() const { return m_Flags & 0x10; }
	unsigned int push() const { return m_Flags & 0x08; }
	unsigned int rst() const { return m_Flags & 0x04; }
	unsigned int syn() const { return m_Flags & 0x02; }
	unsigned int fin() const { return m_Flags & 0x01; }
	unsigned int synfin() const { return m_Flags & 0x03; }
	//---------------------------------------------------------------------------
	/// \brief Проверка контрольной суммы.
	///
	/// \param IPAddrLen_ размер IP адреса в байтах (4 для IPv4, 16 для IPv6)
	//---------------------------------------------------------------------------
	bool checkCRC(unsigned short* IPSrcDst_, int DataSize_, int IPAddrLen_=4) const;
} TTCPHeader;


// -------------------------------------------------------------------
/// \brief Заголовок IP пакета.
///
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|Version|  IHL  |Type of Service|          Total Length          |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|         Identification        |Flags|      Fragment Offset     |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|  Time to Live |    Protocol   |         Header Checksum        |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|                       Source Address                           |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|                    Destination Address                         |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
/// 	|                    Options                    |    Padding     |
/// 	+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// -------------------------------------------------------------------
typedef struct  SIPv4Header
{
	unsigned char m_Version;
	unsigned char m_ToS;
	unsigned short m_TotalLength;
	unsigned short m_Identification;
	unsigned short m_Flags;
	unsigned char m_TTL;
	unsigned char m_Protocol;
	unsigned short m_CRC;
	unsigned int m_IPSrc;
	unsigned int m_IPDst;

	unsigned int version() const { return m_Version >> 4; }
	unsigned int IHL() const { return m_Version & 0x0F; }
	unsigned int headerLength() const { return 4 * IHL(); }
	unsigned int totalLength() const { return ntohs(m_TotalLength); }
	unsigned int payloadLength() const { return totalLength() > headerLength() ? totalLength() - headerLength() : 0; }
	unsigned short identification() const { return ntohs(m_Identification); }
	unsigned int moreFragments() const { return m_Flags & 0x20; }
	unsigned int fragmentOffset() const { return (ntohs(m_Flags) & 0xFFF) * 8; }
	unsigned int isFragment() const { return (m_Flags & 0xFF2F); }
	unsigned __int64 IPSrcDst() const { return *(unsigned __int64*)(&m_IPSrc); }
	//---------------------------------------------------------------------------
	/// \brief Проверка контрольной суммы.
	//---------------------------------------------------------------------------
	inline bool checkCRC() const;
	//---------------------------------------------------------------------------
	/// \brief Проверка корректности заголовка без проверки контрольной суммы.
	//---------------------------------------------------------------------------
	inline bool isValid(unsigned int Length_) const;
} TIPv4Header;


struct pcap_file_header			//24
{
	pcap_file_header()
	{
		magic = 0xA1B2C3D4;
		version_major = 0x0002;
		version_minor = 0x0004;
		thiszone = 0x00000000;
		sigfigs = 0x00000000;
		snaplen = 0x00000100;
		linktype = 0x000000E4;
	} 
	int				magic;
	unsigned short	version_major;
	unsigned short	version_minor;
	int				thiszone;
	unsigned int	sigfigs;
	unsigned int	snaplen;
	unsigned int	linktype;
};

struct pcap_pkthdr			//16 байт
{
	pcap_pkthdr()
	{
		ts.tv_sec = 0;
		ts.tv_usec = 0;
	}
	struct timeval ts;		//8 байт
	unsigned int caplen;	
	unsigned int len;
};

struct SessionsAddr
{
	SessionsAddr()
	{
		m_IPS = 0;
		m_PortS = 0;
		m_IPD = 0;
		m_PortD = 0;
	}
	SessionsAddr(unsigned int IPS, unsigned short	PortS, unsigned int IPD, unsigned short	PortD)
	{
		m_IPS   = IPS;  
		m_PortS = PortS;
		m_IPD   = IPD; 
		m_PortD = PortD;
	}
	unsigned int	m_IPS;
	unsigned short	m_PortS;
	unsigned int	m_IPD;
	unsigned short	m_PortD;
};

struct SonusMainInfo
{
	SonusMainInfo()
	{
		m_ParserID.clear();
		m_pDuplexParser = NULL;
	}
	QString			m_ParserID;					//< Идентификатор парсера через IPS PortS IPD PortD
	unsigned int*	m_pDuplexParser;			//< Указатель на парсер обратного канала
	SessionsAddr	m_ServerSession;			//< Адресная информация
	unsigned int	m_aliveTimer;				//< время последнего появления пакета данной сессии
};


struct SonusLogicalChannel
{
	unsigned int		ID;						//< Идентификатор логического канала
	unsigned int		m_aliveTimer;			//< Время последнего появления пакета данной сессии
	QList<SessionsAddr> UDPSessions;			//< список UDP сессий
	QList<QString>		SIPHeaders;				//< Списко SIP заголовков
	QList<QString>		TelNumbers;				//< Списко Tel номеров
	QString				LogicalChannelsText;	//< Текст, передаваемый по логическим каналам // для отладки


};

//#pragma pack(pop)


#endif
