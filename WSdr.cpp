#include "WSdr.h"
#include "ui_WSdr.h"
#include <QTime>

WSdr::WSdr(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WSdr)
{
    ui->setupUi(this);
    filesForProgress.clear();

    Path = QCoreApplication::applicationDirPath() + "/_InputData";
    QDir dir(Path);
    if(!dir.exists())
        dir.mkdir(Path);
    else
    {
        dir.rmdir(Path);
        dir.mkdir(Path);
    }

    ui->TE_Catalog->clear();
    ui->LW_EndFile->clear();
    ui->LW_Files->clear();
    ui->TE_Catalog->setText(Path);

    Log(QString("Programm start"));

    QStringList path;
    path.append(Path);

    QString str1 = "Path to watch   " + Path;
    Log(str1);

    wartcher = new QFileSystemWatcher(this);
    wartcher->addPath(Path);

    dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::Size | QDir::Reversed);

    QFileInfoList list = dir.entryInfoList();
    for(int i = 0; i < list.size(); i++)
    {
        filesForProgress.append(list.at(i).absoluteFilePath());
        ui->LW_Files->addItem(list.at(i).fileName());
    }
    str1 = "В каталоге найдено %1 файлов для обработки";
    str1 = str1.arg(list.size());
    Log(str1);

    connect(wartcher, SIGNAL(directoryChanged(QString)), this, SLOT(changed(QString)));
    connect(wartcher, SIGNAL(fileChanged     (QString)), this, SLOT(changed(QString)));

    encode = new Encode();
    connect(encode, SIGNAL(Log(QString)), this,  SLOT(Log(QString)));
    connect(encode, SIGNAL(Complited(QString)), this,  SLOT(Complited(QString)));

    m_parse = new Parse(encode);
	connect(m_parse, SIGNAL(Log(QString)), this, SLOT(Log(QString)));
    connect(m_parse, SIGNAL(Restart()),    this, SLOT(changed()));

    connect(m_parse, SIGNAL(setmaxpr(long)),   this, SLOT(SetMaxProgress(long)));
    connect(m_parse, SIGNAL(setvaluepr(long)), this, SLOT(SetValueProgress(long)));

    connect(&timerRestart, SIGNAL(timeout()), this, SLOT(nextFile()));
    connect(&timerRewatch, SIGNAL(timeout()), this, SLOT(changed()));

    SetMaxProgress(1);
    SetValueProgress(0);
    timerRestart.start(5000);
}

WSdr::~WSdr()
{
    delete m_parse;
    delete encode;
    delete ui;
}

void WSdr::changed()
{
    QString str = "";
    changed(str);
}

void WSdr::Log(QString str)
{
    QString str1 = QTime::currentTime().toString() + "   -   "+ str;
    ui->TE_LOG->append(str1);
}

void WSdr::Complited(QString str)
{
    QListWidgetItem *wi = new QListWidgetItem;
    wi->setText(str);
    wi->setForeground(Qt::blue);
    ui->LW_EndFile->addItem(wi);
}

void WSdr::SetMaxProgress(long i)
{
    ui->PB_AllProgress->setMaximum(i);
}

void WSdr::SetValueProgress(long i)
{
    ui->PB_AllProgress->setValue(i);
}

void WSdr::changed(const QString &)
{
    QString str1;
    QStringList oldList, newList;

    for(int i = 0; i < ui->LW_Files->count(); i++)
    {
        QListWidgetItem *item = ui->LW_Files->item(i);
        oldList << item->text();
    }
    qSort(oldList);

    QDir dir(Path);
    dir.setFilter(QDir::Dirs | QDir::Files | QDir::NoDotAndDotDot);
    dir.setSorting(QDir::Size | QDir::Reversed);
    QFileInfoList list = dir.entryInfoList();

    filesForProgress.clear();
    for(int i = 0; i < list.size(); i++)
    {
        filesForProgress.append(list.at(i).absoluteFilePath());
        newList << list.at(i).fileName();
    }
    qSort(newList);

    QSet<QString> intersection;
    if(oldList.count() > newList.count())
    {
        ui->LW_Files->clear();
        intersection = oldList.toSet().intersect(newList.toSet());
        foreach (const QString &value, intersection)
        {
            ui->LW_Files->addItem(value);
        }

        intersection = oldList.toSet().subtract(newList.toSet());
//         foreach (const QString &value, intersection)
//         {
//             str1 = "В каталоге изменения: Обработан файл  " + value;
//             Log(str1);
//         }

        if(!m_parse->isWork())
        {
            nextFile();
        }
    }
    else if(oldList.count() < newList.count())
    {
        intersection = newList.toSet().subtract(oldList.toSet());
        foreach (const QString &value, intersection)
        {
            QListWidgetItem *wi = new QListWidgetItem;
            wi->setText(value);
            wi->setForeground(Qt::blue);
            ui->LW_Files->addItem(wi);

            str1 = "В каталоге изменения: Добавлен файл  " + value;
            Log(str1);
        }

        if(!m_parse->isWork())
        {
            nextFile();
        }
    }
    else if(oldList.count() == newList.count() && oldList.count() == 0 && newList.count() == 0)
    {
        str1 = "В каталоге нет файлов";
        Log(str1);

        timerRewatch.stop();
        timerRewatch.start(10000);
    }
}

void WSdr::nextFile()
{
    if(filesForProgress.count() > 0)
    {
        QFileInfo fileinf(filesForProgress.at(0));
        if(fileinf.isWritable() && fileinf.isReadable() && !m_parse->isWork())
            m_parse->ParseFile(filesForProgress.at(0));
        else
        {
            timerRestart.start(5000);
            QApplication::processEvents();
        }
    }
    else
    {
        timerRewatch.start(5000);
        QApplication::processEvents();
    }
}


void WSdr::on_PB_Start_clicked()
{
    QApplication::processEvents();
    if(!m_parse->isWork())
        nextFile();
}

void WSdr::on_PB_Clear_clicked()
{
    ui->TE_LOG->clear();

    QString str = "\n";
    Log(str);
}
