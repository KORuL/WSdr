#ifndef SND_AUD_H
#define SND_AUD_H

#include <QObject>
#include <QVector>

class SND_AUD : public QObject
{
    Q_OBJECT
public:
    explicit SND_AUD(QObject *parent = 0);

signals:

public slots:
    short *DoSynchr(char *Buf, unsigned int lenght, unsigned int &outlength);
};

#endif // SND_AUD_H
