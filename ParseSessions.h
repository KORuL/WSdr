#ifndef PARSESESSIONS_H
#define PARSESESSIONS_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QCoreApplication>

class ParseSessions : public QObject
{
    Q_OBJECT
public:
    explicit ParseSessions(QString filename, QString sessions, QObject *parent = 0);
    ~ParseSessions();

public:
    void write(unsigned char* Buf, unsigned int len);

signals:
    void WorkEncode(QString &str, QString &filein);

private:
    QFile   file;
    QDir    dir;
    QString Path;
    QString FileName;
    QString filein;
};

#endif // PARSESESSIONS_H
