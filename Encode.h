#ifndef ENCODE_H
#define ENCODE_H

#include <QObject>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include "Adpcm.h"
#include "SND_AUD.h"

class Encode : public QObject
{
    Q_OBJECT
public:
    explicit Encode(QObject *parent = 0);

signals:
    void Log(QString str);
    void Complited(QString str);

public slots:
    void EncodeFile(QString &str, QString &filein);

private:
    SND_AUD m_SND_AUD;
    QString Path;
    QFile   fileOut;
    QFile   fileIn;
    long    cnt;
};

#endif // ENCODE_H
