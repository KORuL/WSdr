#include "Parse.h"

#include "Structs.h"

Parse::Parse(Encode *encode, QObject *parent) : QObject(parent)
{
    m_encode = encode;
    work     = false;
}

void Parse::ParseFile(QString FileName)
{
    QFile InFile(FileName);
    QFileInfo fileinf(FileName);

    QHash<QString, ParseSessions *> SessionsParsers;
    ParseSessions*	pcurSessionsParser;

    if (InFile.open(QIODevice::ReadOnly))
    {
        work = true;
        unsigned int len = InFile.size();
        unsigned char* Buf = new unsigned char[len];

        InFile.read((char*)Buf,len);
        InFile.close();

        unsigned int	Offset = 0,	// pcap header
                pktStart = 24;
        pcap_pkthdr *pkthdr;
        SIPv4Header *IPHdr;
        STCPHeader  *TCPHdr;
        SUDPHeader  *UDPHdr;

        QString AddressStr,ObrAddressStr;

        try {
            QString str2 = "Start Parse " + fileinf.fileName() + "\r\n";
            Q_EMIT Log(str2);
            Q_EMIT setmaxpr(len);
            QApplication::processEvents();

            while (Offset < len)
            {
                Q_EMIT setvaluepr(Offset);
                AddressStr.clear();

                pkthdr = (pcap_pkthdr*)(Buf + pktStart);
                pktStart += 16;
                Offset = pktStart;

                Offset += 14;

                IPHdr = (SIPv4Header*)(Buf + Offset);
                Offset += IPHdr->headerLength();

                //< Если TCP
                if (IPHdr->m_Protocol == 0x06)
                {
                    //<Смещаемся по заголовку TCP
                    TCPHdr = (STCPHeader*)(Buf + Offset);
                    Offset += TCPHdr->headerLength();
                    //< Адресная информация
                    AddressStr = QString("IPSrc_") + ConvertIP  (IPHdr->m_IPSrc)
                            + QString("_PortSrc_") + ConvertPort(TCPHdr->m_Ports.m_PortSrc)
                            + QString("_IPDst_")   + ConvertIP  (IPHdr->m_IPDst)
                            + QString("_PortDst_") + ConvertPort(TCPHdr->m_Ports.m_PortDst);

                    ObrAddressStr = QString("%1_%2_%3_%4").arg(ConvertIP(IPHdr->m_IPDst))
                            .arg(ConvertPort(TCPHdr->m_Ports.m_PortDst))
                            .arg(ConvertIP(IPHdr->m_IPSrc))
                            .arg(ConvertPort(TCPHdr->m_Ports.m_PortSrc));

                    //<
                    //< Если это направление уже обрабатывается
                    //<
                    if (SessionsParsers.contains(AddressStr))
                    {
                        pcurSessionsParser = SessionsParsers[AddressStr];
                        //< Парсим внутренний протокол
                        if(pcurSessionsParser != NULL)
                            pcurSessionsParser->write(Buf + Offset,ntohs(IPHdr->m_TotalLength) - IPHdr->headerLength() - TCPHdr->headerLength());
                        //<
                    }
                    else//< Если это направление новое
                    {
                        char *BufIn = (char*)(Buf + Offset);

                        const char start  = 0x82;
                        const char start1 = 0x7e;

                        const char A = 0x41;
                        const char U = 0x55;
                        const char D = 0x44;

                        const char S = 0x53;
                        const char N = 0x4e;

                        ///< 0x827e
                        if( start == BufIn[0] && start1 == BufIn[1] )
                        {
                            if( (A == BufIn[4] && U == BufIn[5] && D == BufIn[6]) ||
                                    (S == BufIn[4] && N == BufIn[5] && D == BufIn[6]) )
                            {
                                //< Каждый ParseSessions обраюатывает отдельную сессию
                                pcurSessionsParser = new ParseSessions(fileinf.fileName(), AddressStr);
                                //< Добавляю новый парсер в таблицу
                                SessionsParsers.insert(AddressStr, pcurSessionsParser);
                                //< Парсим внутренний протокол
                                pcurSessionsParser->write(Buf + Offset, ntohs(IPHdr->m_TotalLength) - IPHdr->headerLength() - TCPHdr->headerLength());
                                //<
                            }
                            else
                            {
                                pcurSessionsParser = NULL;
                            }
                        }
                        else
                        {
                            pcurSessionsParser = NULL;
                        }
                    }
                }
                else if (IPHdr->m_Protocol == 0x11)
                {
                    UDPHdr = (SUDPHeader*)(Buf + Offset);
                    Offset += UDPHdr->headerLength();
                }

                QApplication::processEvents();
                //<
                pktStart += pkthdr->len;
            }
			delete[] Buf;

			QString str = "Saving Results of Parse " + fileinf.fileName() + "\r\n";
			Q_EMIT Log(str);

			foreach(const ParseSessions *var, SessionsParsers)
			{
				QApplication::processEvents();
				QObject::connect(var, SIGNAL(WorkEncode(QString&, QString&)), m_encode, SLOT(EncodeFile(QString&, QString&)));
				delete var;
			}

            Q_EMIT setmaxpr(1);
            Q_EMIT setvaluepr(0);
            QApplication::processEvents();

			InFile.close();
			bool remove = InFile.remove();
			if (!remove)
			{
				QString str = "Can't open remove File!!!  " + fileinf.fileName() + "\r\n";
				Q_EMIT Log(str);
			}

			work = false;

        } catch (...) {
            delete[] Buf;

			InFile.close();
            bool remove = InFile.remove();
			QString str = "File is BAD!!!  -  " + fileinf.fileName() + "\r\n";
			Q_EMIT Log(str);

            if(!remove)
            {
				QString str = "File is BAD!!! Cannot remove  -  " + fileinf.fileName() + "\r\n";
				Q_EMIT Log(str);
            }

            foreach(const ParseSessions *var, SessionsParsers)
            {
                QApplication::processEvents();
                delete var;
            }

            Q_EMIT setmaxpr(1);
            Q_EMIT setvaluepr(0);
            QApplication::processEvents();

			work = false;
			Q_EMIT Restart();
        }
    }
    else
    {
        Q_EMIT setmaxpr(1);
        Q_EMIT setvaluepr(0);
        QApplication::processEvents();

		InFile.close();
        QString str = "Can't open input File!!!  -  " + fileinf.fileName() + "\r\n";
        Q_EMIT Log(str);
    }
}
