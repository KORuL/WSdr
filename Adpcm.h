#ifndef ADPCM_H
#define ADPCM_H

#include <QObject>

typedef struct ImaState {
    int index = 0;    		// Index into step size table
    int previousValue = 0;	// Most recent sample value
    int pos_clamp, neg_clamp = 0;
} ima_adpcm_state_t;


class Adpcm : public QObject
{
    Q_OBJECT
public:
    explicit Adpcm(QObject *parent = 0);

    static void encode_ima_adpcm_i16_e8(short* input, unsigned char* output, int input_length, ima_adpcm_state_t *state);
    static void encode_ima_adpcm_u8_e8(unsigned char* input, unsigned char* output, int input_length, ima_adpcm_state_t *state);

    static void decode_ima_adpcm_e8_i16(unsigned char* input, short* output, int input_length, ima_adpcm_state_t *state);
    static void decode_ima_adpcm_e8_u8(unsigned char* input, unsigned char* output, int input_length, ima_adpcm_state_t *state);
};

#endif // ADPCM_H
