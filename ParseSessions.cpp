#include "ParseSessions.h"

ParseSessions::ParseSessions(QString filename, QString sessions, QObject *parent) : QObject(parent)
{
    Path = QCoreApplication::applicationDirPath() + "/_OutSessions";
    QDir dir(Path);
    if(!dir.exists())
        dir.mkdir(Path);

    filein = filename;

    Path += "/";
    Path += filename;
    QDir dir1(Path);
    if(!dir1.exists())
        dir1.mkdir(Path);

    file.setFileName(Path + "/" + sessions);
    file.resize(0);
    file.close();

    FileName = Path + "/" + sessions;
}

ParseSessions::~ParseSessions()
{
    file.close();

    QFileInfo fileinf(FileName);

    if(fileinf.size() == 0)
    {
        QFile file1(FileName);
        file1.remove();
    }
    else
        Q_EMIT WorkEncode(FileName, filein);
}

void ParseSessions::write(unsigned char *Buf, unsigned int len)
{
    if(file.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        file.write((char*)Buf, len);
    }
    file.close();
}
